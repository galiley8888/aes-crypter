# K/Ki converter for (Ericsson) AUC/HLR

AES encryption / decryption tool to convert between K and Ki values used in (Ericsson) AUC/HLR

### Usage

More information alailable [here](https://stackpointer.io/telecom/k-ki-key-conversion-tool-auc-hlr/169/)


## Copyright and License
Copyright (c) 2017, Mohamed Ibrahim. Code released under the [MIT](LICENSE) license.


