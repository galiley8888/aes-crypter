#!/usr/bin/perl -w

#
# Rijndael (AES) Encryption / Decryption Tool
# Copyright 2012 Mohamed Ibrahim
#
# This software is provided as-is, without express or implied warranty.
# Permission to use, copy, modify, distribute or sell this software, with or
# without fee, for any purpose and by any individual or organization, is hereby
# granted, provided that the above copyright notice and this paragraph appear
# in all copies. Distribution as a part of an application or binary must
# include the above copyright notice in the documentation and/or other
# materials provided with the application or distribution.
#

use Getopt::Std;
use File::Basename;
use Crypt::Rijndael;
#use Crypt::Rijndael_PP ':all';
#use Crypt::OpenSSL::AES;

%_PROGRAM = (
  'program' => basename($0),
  'author'  => 'ibrahim',
  'version' => '1.02',
  'date'    => '16-Apr-2012',
  'url'     => 'https://stackpointer.io/telecom/k-ki-key-conversion-tool-auc-hlr/169/'
);

%flags = (
  'encrypt' => 0,
  'decrypt' => 0,
  'key'     => 0,
  'size'    => 128,
  'text'    => ''
);

$_DEBUGON = 0;

if( $#ARGV+1 == 0 )
{
  print_help();
  exit(0);
}

getopts('hdes:k:t:',\%options);
if (defined $options{h}) { print_help(); exit(0); }
if (defined $options{e}) { $flags{'encrypt'} = 1; }
if (defined $options{d}) { $flags{'decrypt'} = 1; }
if (defined $options{k}) { $flags{'key'} =  $options{k}; }
if (defined $options{s}) { $flags{'size'} = $options{s}; }
if (defined $options{t}) { $flags{'text'} = $options{t}; }

# check mode of operation
if ( $flags{'encrypt'}!=1 &&  $flags{'decrypt'}!=1 )
{
  print STDERR "ERROR: Mode of operation (-d|-e) need to be defined: encryption (-e), decryption (-d)";
  exit(-1);
}

# check mode of operation
if ( $flags{'encrypt'}==1 &&  $flags{'decrypt'}==1 )
{
  print STDERR "ERROR: Options -d and -e are mutually exclusive";
  exit(-1);
}

# is key specified
if ( !defined $options{k} )
{
  print STDERR "ERROR: Key (-k) must be specified";
  exit(-1);
}

# is key size specified
if ( !defined $options{s} )
{
  print STDERR "ERROR: Key size (-s) must be specified";
  exit(-1);
}

# is plaintext/ciphertext specified
if ( !defined $options{t} )
{
  print STDERR "ERROR: Plaintext/Ciphertext (-t) must be specified";
  exit(-1);
}

# check if key size is valid
if ( $flags{'size'} != 128 && $flags{'size'} != 192 && $flags{'size'} != 256 )
{
  print STDERR "ERROR: Key size (-s) must be set to 128, 192 or 256";
  exit(-1);

}

# check if key size and key length matches
if ( ($flags{'size'}/4)  != (length $flags{'key'}))
{
  print STDERR "ERROR: mismatch in size and key. Expecting key length of " . ($flags{'size'}/4) . " characters";
  exit(-1);
}

# check if key size and plaintext/ciphertext length matches
if ( ($flags{'size'}/4)  != (length $flags{'text'}))
{
  print STDERR "ERROR: mismatch in size and text. Expecting text length of " . ($flags{'size'}/4) . " characters";
  exit(-1);
}

$cipher = Crypt::Rijndael->new( pack("H*",$flags{'key'}), Crypt::Rijndael::MODE_ECB() );
#$cipher = new Crypt::OpenSSL::AES(pack("H*",$flags{'key'}));

#$encrypted = $cipher->encrypt(pack("H32", $plaintext));
#$decrypted = $cipher->decrypt($encrypted);

if($flags{'encrypt'}==1)
{
  $cipher_e = $cipher->encrypt(pack("H*",$flags{'text'}));

  # when using Crypt::Rijndael_PP
  # $cipher_e = rijndael_encrypt($flags{'key'}, MODE_ECB, pack("H32",$flags{'text'}),  128, 128);

  # when using Crypt::OpenSSL::AES
  # $cipher_e = $cipher->encrypt(pack("H*",$flags{'text'}));

  print unpack('H*',$cipher_e) . "\n";
}

if($flags{'decrypt'}==1)
{
  $cipher_d = $cipher->decrypt(pack("H*",$flags{'text'}));

  # when using Crypt::Rijndael_PP
  # $cipher_d = rijndael_decrypt($flags{'key'}, MODE_ECB, pack("H32",$flags{'text'}), 128, 128);

  # when using Crypt::OpenSSL::AES
  # $cipher_d = $cipher->decrypt(pack("H*",$flags{'text'}));

  print unpack('H*',$cipher_d) . "\n";
}

sub print_help
{
  our %_PORGRAM;

  print "\n";
  print << "EOF";

  Usage: $_PROGRAM{'program'} [-e | -d] -s keysize -k key -t text
  AES Encryption/Decryption Tool

  $_PROGRAM{'program'} v$_PROGRAM{'version'} written by $_PROGRAM{'author'} ($_PROGRAM{'url'})

  Options:
  -h    prints this help text

  -e    perform encryption. pass plaintext in -t option.
        this option is mutually exclusive with the -d option

  -d    perform decryption. pass ciphertext in -t option.
        this option is mutually exclusive with the -e option

  -s    key size in bits. valid keys are either 128, 192 and 256 bits

  -k    key in hex format

  -t    plaintext or ciphertext in hex based on encryption/decryption mode


  Example:

  Encrypt data 11111111111111111111111111111111 with key 12345678901234567890123456789012, with keysize of 128 bits
  $_PROGRAM{'program'} -e -s 128 -k 12345678901234567890123456789012 -t 11111111111111111111111111111111

  Decrypt data 699dd4292dc6f02fd1b31c5ac0a4fad3 with key 12345678901234567890123456789012, with keysize of 128 bits
  $_PROGRAM{'program'} -d -s 128 -k 12345678901234567890123456789012 -t 699dd4292dc6f02fd1b31c5ac0a4fad3

EOF
  exit;
}

sub debug_print ($)
{
  my ($msg) = @_;

  print STDERR $msg . "\n" if ($_DEBUGON==1);
}
